from ket import *
from ket import code_ket
from math import pi

bell = cnot(H, I)

@code_ket
def teleport(qubit, bell_pair):
    adj(bell, qubit, bell_pair[0])

    if measure(bell_pair[0]) == 1:
        X(bell_pair[1])

    if measure(qubit) == 1:
        Z(bell_pair[1])


a, b, c = quant(3)

phase(pi/4, H(a))

d_a = dump(a)

bell(b, c)

teleport(a, b+c)

d_c = dump(c)

print('Initial State (a)')
print(d_a.show())

print('-------')

print('Final State (c)')
print(d_c.show())
